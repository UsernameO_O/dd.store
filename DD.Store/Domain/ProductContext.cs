﻿using DD.Store.Domain.Entities;
using DD.Store.Models.Product;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DD.Store.Domain
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options) { }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<ProductAttributesEntity> ProductAttributes { get; set; }
        public DbSet<AttributesEntity> Attributes { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<CategoryAttributesEntity> CategoryAttributes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            

            modelBuilder.Entity<ProductEntity>().HasData(
            new
            {
                Id = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                ImagePath = "/imageproduct/smartphone_1.png",
                Brand = "Infinix",
                Model = "HOT 30I",
                Description = "Телефон бюджетный",
                Price = 8100
            },
            new
            {
                Id = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                ImagePath = "/imageproduct/washingmachine_1.png",
                Brand = "Weissgauff",
                Model = "WM 4606 Steam",
                Description = "Стирает, но не сушит",
                Price = 21300
            },
            new
            {
                Id = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                ImagePath = "/imageproduct/fridge_1.png",
                Brand = "Indesit",
                Model = "DS 4200",
                Price = 26700,
                Description = "И холодит, и морозит"
            },
            new
            {
                Id = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                ImagePath = "/imageproduct/vacuumcleaner_1.png",
                Brand = "Ginzzu",
                Model = "VS420",
                Price = 3600,
                Description = "Стоит копейки"
            });


            modelBuilder.Entity<ProductAttributesEntity>().HasData(
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                AttributeId = new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"),
                Value = "4G"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                AttributeId = new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"),
                Value = "Алюминий"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                AttributeId = new Guid("cd8c3655-d190-481f-94af-4abba625dd15"),
                Value = "6.5"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                AttributeId = new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"),
                Value = "5000"
            },


            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                AttributeId = new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"),
                Value = "Отсутствует"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                AttributeId = new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"),
                Value = "Электронное"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                AttributeId = new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"),
                Value = "Русский"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                AttributeId = new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"),
                Value = "6"
            },


            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                AttributeId = new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"),
                Value = "Сверху"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                AttributeId = new Guid("4a044394-db88-462f-8e28-4f26037696c7"),
                Value = "R600A"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                AttributeId = new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"),
                Value = "6"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                AttributeId = new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"),
                Value = "361"
            },


            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                AttributeId = new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"),
                Value = "Сухая"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                AttributeId = new Guid("131c4218-9c41-4097-92af-6d95b05518a6"),
                Value = "Контейнер"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                AttributeId = new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"),
                Value = "360"
            },
            new
            {
                Id = Guid.NewGuid(),
                ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                AttributeId = new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"),
                Value = "3"
            });



            modelBuilder.Entity<AttributesEntity>().HasData(
            new
            {
                Id = new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"),
                Name = "CommunicationStandard",
                Description = "Стандарт связи",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"),
                Name = "Material",
                Description = "Материал",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("cd8c3655-d190-481f-94af-4abba625dd15"),
                Name = "Diagonal",
                Description = "Диагональ",
                Type = "",
                DataType = "int"
            },
            new
            {
                Id = new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"),
                Name = "Battery",
                Description = "Емкость аккумулятора",
                Type = "мАч",
                DataType = "int"
            },
            new
            {
                Id = new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"),
                Name = "Freezer",
                Description = "Морозильная камера",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("4a044394-db88-462f-8e28-4f26037696c7"),
                Name = "Refrigerant",
                Description = "Хладогент",
                Type = "",
                DataType = "string"
            },
            new 
            {
                Id = new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"),
                Name = "MaxTemperature",
                Description = "Максимальная температура",
                Type = "С",
                DataType = "int"
            },
            new 
            {
                Id = new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"),
                Name = "FreezerVolume",
                Description = "Объем холодильной камеры",
                Type = "л",
                DataType = "int"
            },
            new
            {
                Id = new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"),
                Name = "Drying",
                Description = "Сушка",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"),
                Name = "ControlType",
                Description = "Тип управления",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"),
                Name = "Language",
                Description = "Язык интерфейса",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"),
                Name = "WashingVolume",
                Description = "Загрузка белья",
                Type = "л",
                DataType = "int"
            },
            new
            {
                Id = new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"),
                Name = "TypeCleaning",
                Description = "Тип уборки",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("131c4218-9c41-4097-92af-6d95b05518a6"),
                Name = "TypeContainer",
                Description = "Вид пылесборника",
                Type = "",
                DataType = "string"
            },
            new
            {
                Id = new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"),
                Name = "Power",
                Description = "Мощность всасывания",
                Type = "Вт",
                DataType = "int"
            },
            new
            {
                Id = new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"),
                Name = "CleanerVolume",
                Description = "Объем пылесборника",
                Type = "л",
                DataType = "int"
            });

            modelBuilder.Entity<CategoryAttributesEntity>().HasData(
                new
                {
                    Id = new Guid("2c8ac4cd-5b01-4334-b53e-980d2353e582"),
                    CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                    AttributeId = new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79")
                },
                new
                {
                    Id = new Guid("03f18b6b-3e79-4e19-befe-ac492681cdd0"),
                    CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                    AttributeId = new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031")
                },
                new
                {
                    Id = new Guid("8d17ac93-05c3-485c-bf1f-abfdc7359ae5"),
                    CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                    AttributeId = new Guid("cd8c3655-d190-481f-94af-4abba625dd15")
                },
                new
                {
                    Id = new Guid("42043b4a-ca08-4cc2-8f9a-0c8ff4a031c5"),
                    CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                    AttributeId = new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435")
                },
                new
                {
                    Id = new Guid("8de2866f-18b9-445c-add7-2b5d66aa7b66"),
                    CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                    AttributeId = new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b")
                },
                new
                {
                    Id = new Guid("8dd40c81-6a5a-4bed-8770-3c0003eda6e7"),
                    CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                    AttributeId = new Guid("4a044394-db88-462f-8e28-4f26037696c7")
                },
                new
                {
                    Id = new Guid("2d8459d6-7fdc-48f0-b33b-855a6ede4e42"),
                    CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                    AttributeId = new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad")
                },
                new
                {
                    Id = new Guid("f6d7f1a8-6912-4382-9b1d-daeea9f36b0f"),
                    CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                    AttributeId = new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c")
                },
                new
                {
                    Id = new Guid("f6c92d1d-4d04-4344-9bc5-e616d27bc6ca"),
                    CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                    AttributeId = new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df")
                },
                new
                {
                    Id = new Guid("37e419a8-a8ce-4482-98c9-692860d7f56b"),
                    CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                    AttributeId = new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3")
                },
                new
                {
                    Id = new Guid("9baaaa2f-46b1-4db0-88d3-ac0f3be6a485"),
                    CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                    AttributeId = new Guid("9904b902-22ae-4fad-a436-279f151e0e5f")
                },
                new
                {
                    Id = new Guid("89fd4b91-a365-4158-bdc5-2f00797cd59a"),
                    CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                    AttributeId = new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2")
                },
                new
                {
                    Id = new Guid("ea8aa611-232a-4fe1-87ad-55982a328306"),
                    CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                    AttributeId = new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392")
                },
                new
                {
                    Id = new Guid("b62b47b0-dba6-4790-9598-f64eb50346ab"),
                    CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                    AttributeId = new Guid("131c4218-9c41-4097-92af-6d95b05518a6")
                },
                new
                {
                    Id = new Guid("d5efe5d4-3c20-49a6-b53f-6f18eb86f275"),
                    CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                    AttributeId = new Guid("738e0ca1-97c0-4561-8477-87792197f9fe")
                },
                new
                {
                    Id = new Guid("43bb9480-ff5c-4ae8-9e5d-e05c1fbcda13"),
                    CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                    AttributeId = new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa")
                });

            modelBuilder.Entity<CategoryEntity>().HasData(
            new CategoryEntity
            {
                Id = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                Name = "SmartPhone",
                Description = "Смартфон"
            },
            new CategoryEntity
            {
                Id = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                Name = "WashingMachine",
                Description = "Стиральная машина"
            },
            new CategoryEntity
            {
                Id = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                Name = "Fridge",
                Description = "Холодильник"
            },
            new CategoryEntity
            {
                Id = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                Name = "VacuumCleaner",
                Description = "Пылесос"
            });

        }


    }
}
