﻿namespace DD.Store.Domain.Entities
{
    public class AttributesEntity
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Type { get; set; } // единицы измерений
        public string? DataType { get; set; }

    }
}
