﻿namespace DD.Store.Domain.Entities
{
    public class CategoryAttributesEntity
    {
        public Guid Id { get; set; }
        public CategoryEntity Category { get; set; }
        public AttributesEntity Attribute { get; set; }
    }
}
