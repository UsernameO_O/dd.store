﻿namespace DD.Store.Domain.Entities
{
    public class ProductEntity
    {
        public Guid Id { get; set; }
        public CategoryEntity Category { get; set; }
        public string? ImagePath { get; set; }
        public string? Brand { get; set; }
        public string? Model { get; set; }
        public string? Description { get; set; }
        public int Price { get; set; }
        public ICollection<ProductAttributesEntity>? Attributes { get; set; }
    }
}
