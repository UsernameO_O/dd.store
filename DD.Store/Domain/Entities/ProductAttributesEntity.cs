﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DD.Store.Domain.Entities
{
    public class ProductAttributesEntity
    {
        public Guid Id { get; set; }
        public ProductEntity Product { get; set; }
        public AttributesEntity Attribute { get; set; }
        public string? Value { get; set; }

    }
}
