﻿using DD.Store.Domain.Entities;
using DD.Store.Models.Product;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;

namespace DD.Store.Domain.Repositories
{
    public class ProductRepository
    {
        private ProductContext context;
        public ProductRepository(ProductContext context)
        {
            this.context = context;
        }

        public IEnumerable<ProductModel> GetProduct()
        {
            var listProductView = new List<ProductModel>();
            var products = context.Products.Include(x => x.Category).Include(x => x.Attributes).ThenInclude(x => x.Attribute);

            foreach (var product in products)
            {
                List<ProductAttributes> productAttributes = new List<ProductAttributes>();
                if (product.Attributes != null)
                {
                    productAttributes = product.Attributes.Select(x => new ProductAttributes()
                    {
                        Name = x.Attribute.Name,
                        Value = x.Value,
                        Description = x.Attribute.Description,
                        Type = x.Attribute.Type,
                        DataType = x.Attribute.DataType
                    }).ToList();
                }

                listProductView.Add(new ProductModel()
                {
                    Id = product.Id,
                    Category = product.Category.Name,
                    ImagePath = product.ImagePath,
                    Brand = product.Brand,
                    Model = product.Model,
                    Price = product.Price,
                    Description = product.Description,
                    Attributes = productAttributes
                });
            }
            return listProductView;
        }
        public ProductModel? FindByIdProduct(Guid id)
        {
            return GetProduct()?.FirstOrDefault(x => x.Id == id);
        }
        public void DeleteProduct(Guid id)
        {
            var product = FindByIdProductEntity(id);
            context.Remove(product);
            context.SaveChanges();
        }
        public void SaveProduct(ProductModel model)
        {
            // маппинг модели в сущность бд
            ProductEntity entity = new ProductEntity()
            {
                Id = model.Id,
                Category = FindByNameCategory(model.Category),
                ImagePath = model.ImagePath,
                Brand = model.Brand,
                Model = model.Model,
                Description = model.Description,
                Price = model.Price
            };

            List<ProductAttributesEntity> attributes = new List<ProductAttributesEntity>();
            foreach (var attribute in model.Attributes)
            {
                attributes.Add(new ProductAttributesEntity()
                {
                    Id = Guid.NewGuid(),
                    Product = entity,
                    Attribute = FindByNameAttribut(attribute.Name),
                    Value = attribute.Value
                });
            }
            if (FindByIdProductEntity(entity.Id) != null)
            {
                DeleteProduct(entity.Id);
            }

            context.Entry(entity).State = EntityState.Added;
            foreach (var attribute in attributes)
            {
                context.Entry(attribute).State = EntityState.Added;
            }
            context.SaveChanges();
        }

        public List<CategoryModel>? GetCategory()
        {
            var entitysCategories = context.Categories.Include(x => x.Attributes).ThenInclude(x => x.Attribute);
            var categoryModels = new List<CategoryModel>();
            foreach (var entity in entitysCategories)
            {
                List<AttributesEntity> attributes = new List<AttributesEntity>();
                for (int i = 0; i < entity.Attributes.Count; i++)
                {
                    attributes.Add(entity.Attributes[i].Attribute);
                }
                attributes = attributes.OrderBy(x => x.Name).ToList();
                categoryModels.Add(new CategoryModel()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    Attributes = attributes
                });
            }
            return categoryModels;
        }
        //----------------------------Категории------------------------------------------------------------

        // мда
        public void SaveCategory(CategoryModel model)
        {
            var entity = new CategoryEntity()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
            };
            List<AttributesEntity> attributes = new List<AttributesEntity>();
            List<CategoryAttributesEntity> attributesCategory = new List<CategoryAttributesEntity>();
            foreach (var attribute in model.Attributes)
            {
                var attr = new AttributesEntity()
                {
                    Id = attribute.Id,
                    //Category = FindByNameCategory(entity.Name),
                    Name = attribute.Name,
                    Description = attribute.Description,
                    Type = attribute.Type,
                    DataType = attribute.DataType
                };
                attributes.Add(attr);
                attributesCategory.Add(new CategoryAttributesEntity()
                {
                    Id = Guid.NewGuid(),
                    Category = entity,
                    Attribute = attr
                });
            }
            if (context.Categories.FirstOrDefault(x => x.Id == entity.Id) != null)
            {
                context.Entry(entity).State = EntityState.Modified;
                // атрибуты не сохраняются :)
            }
            else
            {
                context.Entry(entity).State = EntityState.Added;
                foreach (var attribute in attributesCategory)
                {
                    context.Entry(attribute).State = EntityState.Added;
                }
                foreach (var attribute in attributes)
                {
                    context.Entry(attribute).State = EntityState.Added;
                }
            }
            context.SaveChanges();
        }

        public void DeleteCategory(Guid id)
        {
            var category = context.Categories.Include(x => x.Attributes).ThenInclude(x => x.Attribute).FirstOrDefault(x => x.Id == id);
            for (int i = 0; i < category.Attributes.Count; i++)
            {
                context.Remove(category.Attributes[i].Attribute);
            }
            context.Remove(category);
            context.SaveChanges();
        }

        private CategoryEntity FindByIdCategoryEntity(Guid id)
        {
            return context.Categories.FirstOrDefault(x => x.Id == id);
        }

        // передает категорию по названию
        private CategoryEntity FindByNameCategory(string name)
        {
            return context.Categories?.FirstOrDefault(x => x.Name == name);
        }
        #region методы для маппинга
        // передает модели атрибутов продукта
        //private List<ProductAttributes> GetAttributesProduct(Guid id)
        //{
        //    List<ProductAttributes> attributes = new List<ProductAttributes>();
        //    var productAttributes = context.ProductAttributes
        //        .Include(x => x.Product)
        //        .ThenInclude(x => x.Category)
        //        .Include(x => x.Attribute)
        //        .Where(x => x.Product.Id == id);

        //    foreach (var attribut in productAttributes)
        //    {
        //        attributes.Add(new ProductAttributes()
        //        {
        //            Name = attribut.Attribute.Name,
        //            Value = attribut.Value,
        //            Description = attribut.Attribute.Description,
        //            Type = attribut.Attribute.Type,
        //            DataType = attribut.Attribute.DataType
        //        });
        //    }
        //    return attributes;
        //}

        // передает сущность продукта
        private ProductEntity FindByIdProductEntity(Guid id)
        {
            return context.Products.FirstOrDefault(x => x.Id == id);
        }
        // поиск нужного атрибута по названию модели
        private AttributesEntity FindByNameAttribut(string name)
        {
            var attributes = context.Attributes?.FirstOrDefault(x => x.Name == name);
            return attributes;
        }
        #endregion
    }
}
