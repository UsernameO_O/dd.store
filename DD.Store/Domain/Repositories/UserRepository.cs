﻿using DD.Store.Domain.Entities;
using DD.Store.Models.Account;
using DD.Store.Models.Service;
using DD.Store.Models.View;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Numerics;

namespace DD.Store.Domain.Repositories
{
    public class UserRepository
    {
        private UserContext context;

        public UserRepository(UserContext context)
        {
            this.context = context;
        }
        public List<UserModel>? GetUser()
        {
            List<UserModel> userModel = new List<UserModel>();
            foreach (var user in context.Users)
            {
                userModel.Add(new UserModel()
                {
                    Id = user.Id,
                    Login = user.Login,
                    Role = user.Role,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Phone = user.Phone
                });
            }
            return userModel;
        }

        public UserModel? FindByIdUser(Guid id)
        {
            var user = context.Users?.FirstOrDefault(x => x.Id == id);
            return new UserModel() 
            {
                Id = user.Id,
                Login = user.Login,
                Role = user.Role,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.Phone
            };
        }

        public bool FindByLoginUser(string login)
        {
            return context.Users?.FirstOrDefault(x => x.Login == login) != null;
        }

        public UserModel FindByLoginUser(string login, string password)
        {
            var user = context.Users?.FirstOrDefault(x => x.Login == login);
            if (user.Password == Hash.Encryption(password))
            { 
                return new UserModel() 
                {
                    Id = user.Id,
                    Login = user.Login,
                    Role = user.Role,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Phone = user.Phone
                };
            }
            else
            {
                return null;
            }
        }

        public void CreateUser(UserViewModel model)
        {
            var user = new UserEntity()
            {
                Id = Guid.NewGuid(),
                Login = model.Login,
                Role = "User",
                FirstName = model.FirstName,
                LastName = model.LastName,
                Phone = model.Phone,
                Password = Hash.Encryption(model.Password)
            };
            context.Entry(user).State = EntityState.Added;
            context.SaveChanges();
        }
        public void UpdateUser(UserModel model)
        {
            var entity = context.Users?.FirstOrDefault(x => x.Id == model.Id);
            if(entity!=null)
            { 
                var user = new UserEntity() 
                {
                    Id = model.Id,
                    Login = model.Login,
                    Role = model.Role,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Phone,
                    Password = entity.Password
                };
                context.Entry(user).State = EntityState.Modified;
                context.SaveChanges();
            }
            else { }
        }

        public void DeleteUser(Guid id)
        {
            var user = context.Users?.FirstOrDefault(x => x.Id == id);
            context.Users.Remove(user);
            context.SaveChanges();
        }
    }
}
