﻿using DD.Store.Domain.Entities;
using DD.Store.Models.Service;
using Microsoft.EntityFrameworkCore;

namespace DD.Store.Domain
{
    public class UserContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public UserContext(DbContextOptions<UserContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().HasData(new UserEntity
            {
                Id = Guid.NewGuid(),
                Login = "main",
                Password = Hash.Encryption("cjkywt"),
                Role = "Main",
                FirstName = "Main",
                LastName = "",
                Phone = ""
            });
        }
    }
}
