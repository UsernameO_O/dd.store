﻿// <auto-generated />
using System;
using DD.Store.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace DD.Store.Migrations
{
    [DbContext(typeof(ProductContext))]
    partial class ProductContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.12")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("DD.Store.Domain.Entities.AttributesEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("DataType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Attributes");

                    b.HasData(
                        new
                        {
                            Id = new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"),
                            DataType = "string",
                            Description = "Стандарт связи",
                            Name = "CommunicationStandard",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"),
                            DataType = "string",
                            Description = "Материал",
                            Name = "Material",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("cd8c3655-d190-481f-94af-4abba625dd15"),
                            DataType = "int",
                            Description = "Диагональ",
                            Name = "Diagonal",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"),
                            DataType = "int",
                            Description = "Емкость аккумулятора",
                            Name = "Battery",
                            Type = "мАч"
                        },
                        new
                        {
                            Id = new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"),
                            DataType = "string",
                            Description = "Морозильная камера",
                            Name = "Freezer",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("4a044394-db88-462f-8e28-4f26037696c7"),
                            DataType = "string",
                            Description = "Хладогент",
                            Name = "Refrigerant",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"),
                            DataType = "int",
                            Description = "Максимальная температура",
                            Name = "MaxTemperature",
                            Type = "С"
                        },
                        new
                        {
                            Id = new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"),
                            DataType = "int",
                            Description = "Объем холодильной камеры",
                            Name = "FreezerVolume",
                            Type = "л"
                        },
                        new
                        {
                            Id = new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"),
                            DataType = "string",
                            Description = "Сушка",
                            Name = "Drying",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"),
                            DataType = "string",
                            Description = "Тип управления",
                            Name = "ControlType",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"),
                            DataType = "string",
                            Description = "Язык интерфейса",
                            Name = "Language",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"),
                            DataType = "int",
                            Description = "Загрузка белья",
                            Name = "WashingVolume",
                            Type = "л"
                        },
                        new
                        {
                            Id = new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"),
                            DataType = "string",
                            Description = "Тип уборки",
                            Name = "TypeCleaning",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("131c4218-9c41-4097-92af-6d95b05518a6"),
                            DataType = "string",
                            Description = "Вид пылесборника",
                            Name = "TypeContainer",
                            Type = ""
                        },
                        new
                        {
                            Id = new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"),
                            DataType = "int",
                            Description = "Мощность всасывания",
                            Name = "Power",
                            Type = "Вт"
                        },
                        new
                        {
                            Id = new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"),
                            DataType = "int",
                            Description = "Объем пылесборника",
                            Name = "CleanerVolume",
                            Type = "л"
                        });
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.CategoryAttributesEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("AttributeId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("CategoryId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("AttributeId");

                    b.HasIndex("CategoryId");

                    b.ToTable("CategoryAttributes");

                    b.HasData(
                        new
                        {
                            Id = new Guid("2c8ac4cd-5b01-4334-b53e-980d2353e582"),
                            AttributeId = new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"),
                            CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f")
                        },
                        new
                        {
                            Id = new Guid("03f18b6b-3e79-4e19-befe-ac492681cdd0"),
                            AttributeId = new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"),
                            CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f")
                        },
                        new
                        {
                            Id = new Guid("8d17ac93-05c3-485c-bf1f-abfdc7359ae5"),
                            AttributeId = new Guid("cd8c3655-d190-481f-94af-4abba625dd15"),
                            CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f")
                        },
                        new
                        {
                            Id = new Guid("42043b4a-ca08-4cc2-8f9a-0c8ff4a031c5"),
                            AttributeId = new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"),
                            CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f")
                        },
                        new
                        {
                            Id = new Guid("8de2866f-18b9-445c-add7-2b5d66aa7b66"),
                            AttributeId = new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"),
                            CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941")
                        },
                        new
                        {
                            Id = new Guid("8dd40c81-6a5a-4bed-8770-3c0003eda6e7"),
                            AttributeId = new Guid("4a044394-db88-462f-8e28-4f26037696c7"),
                            CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941")
                        },
                        new
                        {
                            Id = new Guid("2d8459d6-7fdc-48f0-b33b-855a6ede4e42"),
                            AttributeId = new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"),
                            CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941")
                        },
                        new
                        {
                            Id = new Guid("f6d7f1a8-6912-4382-9b1d-daeea9f36b0f"),
                            AttributeId = new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"),
                            CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941")
                        },
                        new
                        {
                            Id = new Guid("f6c92d1d-4d04-4344-9bc5-e616d27bc6ca"),
                            AttributeId = new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"),
                            CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da")
                        },
                        new
                        {
                            Id = new Guid("37e419a8-a8ce-4482-98c9-692860d7f56b"),
                            AttributeId = new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"),
                            CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da")
                        },
                        new
                        {
                            Id = new Guid("9baaaa2f-46b1-4db0-88d3-ac0f3be6a485"),
                            AttributeId = new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"),
                            CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da")
                        },
                        new
                        {
                            Id = new Guid("89fd4b91-a365-4158-bdc5-2f00797cd59a"),
                            AttributeId = new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"),
                            CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da")
                        },
                        new
                        {
                            Id = new Guid("ea8aa611-232a-4fe1-87ad-55982a328306"),
                            AttributeId = new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"),
                            CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0")
                        },
                        new
                        {
                            Id = new Guid("b62b47b0-dba6-4790-9598-f64eb50346ab"),
                            AttributeId = new Guid("131c4218-9c41-4097-92af-6d95b05518a6"),
                            CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0")
                        },
                        new
                        {
                            Id = new Guid("d5efe5d4-3c20-49a6-b53f-6f18eb86f275"),
                            AttributeId = new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"),
                            CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0")
                        },
                        new
                        {
                            Id = new Guid("43bb9480-ff5c-4ae8-9e5d-e05c1fbcda13"),
                            AttributeId = new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"),
                            CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0")
                        });
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.CategoryEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Categories");

                    b.HasData(
                        new
                        {
                            Id = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                            Description = "Смартфон",
                            Name = "SmartPhone"
                        },
                        new
                        {
                            Id = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                            Description = "Стиральная машина",
                            Name = "WashingMachine"
                        },
                        new
                        {
                            Id = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                            Description = "Холодильник",
                            Name = "Fridge"
                        },
                        new
                        {
                            Id = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                            Description = "Пылесос",
                            Name = "VacuumCleaner"
                        });
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.ProductAttributesEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("AttributeId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ProductId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("AttributeId");

                    b.HasIndex("ProductId");

                    b.ToTable("ProductAttributes");

                    b.HasData(
                        new
                        {
                            Id = new Guid("c76803fd-65fe-49a8-b26c-bdc0c588081b"),
                            AttributeId = new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"),
                            ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                            Value = "4G"
                        },
                        new
                        {
                            Id = new Guid("6b05c73d-3c96-4b65-846f-d53ba31bba42"),
                            AttributeId = new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"),
                            ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                            Value = "Алюминий"
                        },
                        new
                        {
                            Id = new Guid("ab371e48-d879-428f-91b5-f917c9e7d7ac"),
                            AttributeId = new Guid("cd8c3655-d190-481f-94af-4abba625dd15"),
                            ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                            Value = "6.5"
                        },
                        new
                        {
                            Id = new Guid("bc926c45-b029-44fb-b812-a70e987a60e9"),
                            AttributeId = new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"),
                            ProductId = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                            Value = "5000"
                        },
                        new
                        {
                            Id = new Guid("2850a916-322a-4ed9-a60a-f2d1c6b435c9"),
                            AttributeId = new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"),
                            ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                            Value = "Отсутствует"
                        },
                        new
                        {
                            Id = new Guid("47d62abb-cf8f-47b4-89b0-2807a06feea5"),
                            AttributeId = new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"),
                            ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                            Value = "Электронное"
                        },
                        new
                        {
                            Id = new Guid("41ef475c-f0e6-4ef4-9752-031f7307413d"),
                            AttributeId = new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"),
                            ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                            Value = "Русский"
                        },
                        new
                        {
                            Id = new Guid("1f2c31c9-ee20-422f-aeef-e1f8899d5391"),
                            AttributeId = new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"),
                            ProductId = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                            Value = "6"
                        },
                        new
                        {
                            Id = new Guid("f448c3a0-269c-4acb-b75a-5bb649461800"),
                            AttributeId = new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"),
                            ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                            Value = "Сверху"
                        },
                        new
                        {
                            Id = new Guid("cab9f352-3925-47c7-ab8a-80d23816ea75"),
                            AttributeId = new Guid("4a044394-db88-462f-8e28-4f26037696c7"),
                            ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                            Value = "R600A"
                        },
                        new
                        {
                            Id = new Guid("b048fd47-67c8-497d-9543-3a36bf343e06"),
                            AttributeId = new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"),
                            ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                            Value = "6"
                        },
                        new
                        {
                            Id = new Guid("bc182a0a-d488-44ad-9a80-eee140660926"),
                            AttributeId = new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"),
                            ProductId = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                            Value = "361"
                        },
                        new
                        {
                            Id = new Guid("08264d8a-afa2-4517-ac92-468814d399f7"),
                            AttributeId = new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"),
                            ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                            Value = "Сухая"
                        },
                        new
                        {
                            Id = new Guid("e7310f89-70f7-4e5c-9b38-e7b9c941b2a4"),
                            AttributeId = new Guid("131c4218-9c41-4097-92af-6d95b05518a6"),
                            ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                            Value = "Контейнер"
                        },
                        new
                        {
                            Id = new Guid("08277a1c-6b50-4b16-ad5d-0475d931430f"),
                            AttributeId = new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"),
                            ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                            Value = "360"
                        },
                        new
                        {
                            Id = new Guid("77d7e5de-5786-4be5-a40a-0f289259c121"),
                            AttributeId = new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"),
                            ProductId = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                            Value = "3"
                        });
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.ProductEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Brand")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("CategoryId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ImagePath")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Model")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Price")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.ToTable("Products");

                    b.HasData(
                        new
                        {
                            Id = new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"),
                            Brand = "Infinix",
                            CategoryId = new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"),
                            Description = "Телефон бюджетный",
                            ImagePath = "/imageproduct/smartphone_1.png",
                            Model = "HOT 30I",
                            Price = 8100
                        },
                        new
                        {
                            Id = new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"),
                            Brand = "Weissgauff",
                            CategoryId = new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"),
                            Description = "Стирает, но не сушит",
                            ImagePath = "/imageproduct/washingmachine_1.png",
                            Model = "WM 4606 Steam",
                            Price = 21300
                        },
                        new
                        {
                            Id = new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"),
                            Brand = "Indesit",
                            CategoryId = new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"),
                            Description = "И холодит, и морозит",
                            ImagePath = "/imageproduct/fridge_1.png",
                            Model = "DS 4200",
                            Price = 26700
                        },
                        new
                        {
                            Id = new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"),
                            Brand = "Ginzzu",
                            CategoryId = new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"),
                            Description = "Стоит копейки",
                            ImagePath = "/imageproduct/vacuumcleaner_1.png",
                            Model = "VS420",
                            Price = 3600
                        });
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.CategoryAttributesEntity", b =>
                {
                    b.HasOne("DD.Store.Domain.Entities.AttributesEntity", "Attribute")
                        .WithMany()
                        .HasForeignKey("AttributeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DD.Store.Domain.Entities.CategoryEntity", "Category")
                        .WithMany("Attributes")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Attribute");

                    b.Navigation("Category");
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.ProductAttributesEntity", b =>
                {
                    b.HasOne("DD.Store.Domain.Entities.AttributesEntity", "Attribute")
                        .WithMany()
                        .HasForeignKey("AttributeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DD.Store.Domain.Entities.ProductEntity", "Product")
                        .WithMany("Attributes")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Attribute");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.ProductEntity", b =>
                {
                    b.HasOne("DD.Store.Domain.Entities.CategoryEntity", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Category");
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.CategoryEntity", b =>
                {
                    b.Navigation("Attributes");
                });

            modelBuilder.Entity("DD.Store.Domain.Entities.ProductEntity", b =>
                {
                    b.Navigation("Attributes");
                });
#pragma warning restore 612, 618
        }
    }
}
