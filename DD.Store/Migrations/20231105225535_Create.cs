﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace DD.Store.Migrations
{
    /// <inheritdoc />
    public partial class Create : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Attributes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataType = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attributes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryAttributes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AttributeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryAttributes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryAttributes_Attributes_AttributeId",
                        column: x => x.AttributeId,
                        principalTable: "Attributes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryAttributes_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ImagePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Brand = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Model = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductAttributes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AttributeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAttributes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductAttributes_Attributes_AttributeId",
                        column: x => x.AttributeId,
                        principalTable: "Attributes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductAttributes_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Attributes",
                columns: new[] { "Id", "DataType", "Description", "Name", "Type" },
                values: new object[,]
                {
                    { new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"), "string", "Стандарт связи", "CommunicationStandard", "" },
                    { new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"), "string", "Морозильная камера", "Freezer", "" },
                    { new Guid("131c4218-9c41-4097-92af-6d95b05518a6"), "string", "Вид пылесборника", "TypeContainer", "" },
                    { new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"), "string", "Тип уборки", "TypeCleaning", "" },
                    { new Guid("4a044394-db88-462f-8e28-4f26037696c7"), "string", "Хладогент", "Refrigerant", "" },
                    { new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"), "string", "Сушка", "Drying", "" },
                    { new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"), "int", "Мощность всасывания", "Power", "Вт" },
                    { new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"), "int", "Максимальная температура", "MaxTemperature", "С" },
                    { new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"), "int", "Емкость аккумулятора", "Battery", "мАч" },
                    { new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"), "int", "Загрузка белья", "WashingVolume", "л" },
                    { new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"), "string", "Язык интерфейса", "Language", "" },
                    { new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"), "int", "Объем холодильной камеры", "FreezerVolume", "л" },
                    { new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"), "string", "Материал", "Material", "" },
                    { new Guid("cd8c3655-d190-481f-94af-4abba625dd15"), "int", "Диагональ", "Diagonal", "" },
                    { new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"), "string", "Тип управления", "ControlType", "" },
                    { new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"), "int", "Объем пылесборника", "CleanerVolume", "л" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"), "Пылесос", "VacuumCleaner" },
                    { new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"), "Стиральная машина", "WashingMachine" },
                    { new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"), "Холодильник", "Fridge" },
                    { new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"), "Смартфон", "SmartPhone" }
                });

            migrationBuilder.InsertData(
                table: "CategoryAttributes",
                columns: new[] { "Id", "AttributeId", "CategoryId" },
                values: new object[,]
                {
                    { new Guid("03f18b6b-3e79-4e19-befe-ac492681cdd0"), new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"), new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f") },
                    { new Guid("2c8ac4cd-5b01-4334-b53e-980d2353e582"), new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"), new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f") },
                    { new Guid("2d8459d6-7fdc-48f0-b33b-855a6ede4e42"), new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"), new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941") },
                    { new Guid("37e419a8-a8ce-4482-98c9-692860d7f56b"), new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"), new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da") },
                    { new Guid("42043b4a-ca08-4cc2-8f9a-0c8ff4a031c5"), new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"), new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f") },
                    { new Guid("43bb9480-ff5c-4ae8-9e5d-e05c1fbcda13"), new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"), new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0") },
                    { new Guid("89fd4b91-a365-4158-bdc5-2f00797cd59a"), new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"), new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da") },
                    { new Guid("8d17ac93-05c3-485c-bf1f-abfdc7359ae5"), new Guid("cd8c3655-d190-481f-94af-4abba625dd15"), new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f") },
                    { new Guid("8dd40c81-6a5a-4bed-8770-3c0003eda6e7"), new Guid("4a044394-db88-462f-8e28-4f26037696c7"), new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941") },
                    { new Guid("8de2866f-18b9-445c-add7-2b5d66aa7b66"), new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"), new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941") },
                    { new Guid("9baaaa2f-46b1-4db0-88d3-ac0f3be6a485"), new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"), new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da") },
                    { new Guid("b62b47b0-dba6-4790-9598-f64eb50346ab"), new Guid("131c4218-9c41-4097-92af-6d95b05518a6"), new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0") },
                    { new Guid("d5efe5d4-3c20-49a6-b53f-6f18eb86f275"), new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"), new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0") },
                    { new Guid("ea8aa611-232a-4fe1-87ad-55982a328306"), new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"), new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0") },
                    { new Guid("f6c92d1d-4d04-4344-9bc5-e616d27bc6ca"), new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"), new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da") },
                    { new Guid("f6d7f1a8-6912-4382-9b1d-daeea9f36b0f"), new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"), new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941") }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Brand", "CategoryId", "Description", "ImagePath", "Model", "Price" },
                values: new object[,]
                {
                    { new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"), "Weissgauff", new Guid("34daec0b-af9d-46ae-b19c-ac6cdfc962da"), "Стирает, но не сушит", "/imageproduct/washingmachine_1.png", "WM 4606 Steam", 21300 },
                    { new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"), "Ginzzu", new Guid("03d6d0b7-d0ec-40d2-8beb-b94f6e93cce0"), "Стоит копейки", "/imageproduct/vacuumcleaner_1.png", "VS420", 3600 },
                    { new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"), "Indesit", new Guid("b03f0bf1-a0e2-4b6d-b127-f843ab812941"), "И холодит, и морозит", "/imageproduct/fridge_1.png", "DS 4200", 26700 },
                    { new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"), "Infinix", new Guid("d0fb024e-7f8a-4fd9-9cf7-cf8f4259488f"), "Телефон бюджетный", "/imageproduct/smartphone_1.png", "HOT 30I", 8100 }
                });

            migrationBuilder.InsertData(
                table: "ProductAttributes",
                columns: new[] { "Id", "AttributeId", "ProductId", "Value" },
                values: new object[,]
                {
                    { new Guid("08264d8a-afa2-4517-ac92-468814d399f7"), new Guid("1c2797f4-f973-431a-811f-b9d3aaf0d392"), new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"), "Сухая" },
                    { new Guid("08277a1c-6b50-4b16-ad5d-0475d931430f"), new Guid("738e0ca1-97c0-4561-8477-87792197f9fe"), new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"), "360" },
                    { new Guid("1f2c31c9-ee20-422f-aeef-e1f8899d5391"), new Guid("82b16dee-9e63-4ec8-b57e-1ea3fc7468a2"), new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"), "6" },
                    { new Guid("2850a916-322a-4ed9-a60a-f2d1c6b435c9"), new Guid("57ab30c7-1923-40e6-941e-e5c52422c5df"), new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"), "Отсутствует" },
                    { new Guid("41ef475c-f0e6-4ef4-9752-031f7307413d"), new Guid("9904b902-22ae-4fad-a436-279f151e0e5f"), new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"), "Русский" },
                    { new Guid("47d62abb-cf8f-47b4-89b0-2807a06feea5"), new Guid("e2f3361f-87be-4375-bba3-0050ae3471f3"), new Guid("76ddc547-f53f-42c4-8f65-a3beb462b571"), "Электронное" },
                    { new Guid("6b05c73d-3c96-4b65-846f-d53ba31bba42"), new Guid("b95e63a2-4119-45ca-bb4e-3ff6d2f1b031"), new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"), "Алюминий" },
                    { new Guid("77d7e5de-5786-4be5-a40a-0f289259c121"), new Guid("ffcf404d-b1ae-4502-a1fc-8ed000d868fa"), new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"), "3" },
                    { new Guid("ab371e48-d879-428f-91b5-f917c9e7d7ac"), new Guid("cd8c3655-d190-481f-94af-4abba625dd15"), new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"), "6.5" },
                    { new Guid("b048fd47-67c8-497d-9543-3a36bf343e06"), new Guid("7eb1d4f2-684a-467a-98e2-a5941f3f38ad"), new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"), "6" },
                    { new Guid("bc182a0a-d488-44ad-9a80-eee140660926"), new Guid("a3b2289e-10e6-4804-84a1-b8b47d82ab7c"), new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"), "361" },
                    { new Guid("bc926c45-b029-44fb-b812-a70e987a60e9"), new Guid("7ef34cb3-5f61-4256-b8ae-b8df3abc9435"), new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"), "5000" },
                    { new Guid("c76803fd-65fe-49a8-b26c-bdc0c588081b"), new Guid("04215f96-7f97-4626-b6b1-0c60a466dd79"), new Guid("dc1f21fa-d389-4f70-b9a8-e24e253a40f3"), "4G" },
                    { new Guid("cab9f352-3925-47c7-ab8a-80d23816ea75"), new Guid("4a044394-db88-462f-8e28-4f26037696c7"), new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"), "R600A" },
                    { new Guid("e7310f89-70f7-4e5c-9b38-e7b9c941b2a4"), new Guid("131c4218-9c41-4097-92af-6d95b05518a6"), new Guid("c6292ef5-23c3-4c95-a2f4-1c2333fc1e25"), "Контейнер" },
                    { new Guid("f448c3a0-269c-4acb-b75a-5bb649461800"), new Guid("129e1652-b1b1-44d5-a588-f840c9394d3b"), new Guid("c69b089d-fe4f-45d3-92c8-8e4f1a8286fa"), "Сверху" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAttributes_AttributeId",
                table: "CategoryAttributes",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAttributes_CategoryId",
                table: "CategoryAttributes",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributes_AttributeId",
                table: "ProductAttributes",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributes_ProductId",
                table: "ProductAttributes",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryAttributes");

            migrationBuilder.DropTable(
                name: "ProductAttributes");

            migrationBuilder.DropTable(
                name: "Attributes");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
