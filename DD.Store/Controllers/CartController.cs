﻿using DD.Store.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using Newtonsoft.Json;
using System.Collections;
using System.Diagnostics;
using DD.Store.Models.Product;
using DD.Store.Models.Cart;
using DD.Store.Models.Service;
using DD.Store.Models.View;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using DD.Store.Domain.Entities;

namespace DD.Store.Controllers
{
    public class CartController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ProductRepository _repository;
        public CartController(ProductRepository repository, ILogger<HomeController> logger)
        {
            _logger = logger;
            this._repository = repository;
        }

        public IActionResult Index()
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            var order = new OrderModel() {
                Client = new ClientViewModel(),
                Cart = cart,
                isValid = true
            };
            var user = HttpContext.Session.Get<UserEntity>("User") ?? new UserEntity();
            if (user.Id != new Guid("00000000-0000-0000-0000-000000000000"))
            {
                order.Client.FirstName = user.FirstName;
                order.Client.LastName = user.LastName;
                order.Client.Phone = user.Phone;
            }
            return View(order);
        }

        [HttpPost]
        public IActionResult Index(OrderModel model)
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            model.Cart = cart;
            ModelState.Clear();
            if (model.Client.FirstName == null)
            {
                ModelState.AddModelError("Client.FirstName", "Введите имя");
            }
            if (model.Client.LastName == null)
            {
                ModelState.AddModelError("Client.LastName", "Введите фамилию");
            }
            if (model.Client.Address == null)
            {
                ModelState.AddModelError("Client.Address", "Введите адресс");
            }
            if (model.Client.Phone == null)
            {
                ModelState.AddModelError("Client.Phone", "Введите телефон");
            }
            
            if (ModelState.IsValid)
            {
                //HttpContext.Session.Set<Cart>("Cart", new Cart());
                TempData["Order"] = JsonConvert.SerializeObject(model);
                return RedirectToRoute("default", new { controller = "Cart", action = "Order" });
            }
            else
            {
                
                model.isValid = false;
                return View(model);
                
            }
        }

        public IActionResult Order()
        {
            var order = JsonConvert.DeserializeObject<OrderModel>(TempData["Order"].ToString());
            return View(order);
        }

        [HttpGet]
        public JsonResult GetCart()
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            return Json(cart.CartItems);
        }

        [HttpPost]
        public void NumberDown(Guid id)
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            CartItemModel cartItem = cart.FindByProductIdCarts(id);
            cartItem.NumberDown();
            HttpContext.Session.Set<Cart>("Cart", cart);
        }

        [HttpPost]
        public void NumberUp(Guid id)
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            CartItemModel cartItem = cart.FindByProductIdCarts(id);
            cartItem.NumberUp();
            HttpContext.Session.Set<Cart>("Cart", cart);
        }
        [HttpPost]
        public void NumberSet(Guid id, int value)
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            CartItemModel cartItem = cart.FindByProductIdCarts(id);
            cartItem.Number= value;
            HttpContext.Session.Set<Cart>("Cart", cart);
        }
        [HttpPost]
        public void Create(Guid id)
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            ProductModel product = _repository.FindByIdProduct(id);
            cart.Add(new CartItemModel { Id = Guid.NewGuid(), Number = 1, Product = product });
            HttpContext.Session.Set<Cart>("Cart", cart);
        }

        [HttpPost]
        public void Delete(Guid id)
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart") ?? new Cart();
            ProductModel product = _repository.FindByIdProduct(id);
            cart.Delete(product.Id);
            HttpContext.Session.Set<Cart>("Cart", cart);
        }
    }
}
