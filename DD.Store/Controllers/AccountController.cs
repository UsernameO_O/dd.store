﻿using DD.Store.Domain.Entities;
using DD.Store.Domain.Repositories;
using DD.Store.Models;
using DD.Store.Models.Account;
using DD.Store.Models.Cart;
using DD.Store.Models.Service;
using DD.Store.Models.View;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DD.Store.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private UserRepository _repository;
        public AccountController(UserRepository repository, ILogger<HomeController> logger)
        {
            _logger = logger;
            this._repository = repository;
        }

        public UserEntity GetUser()
        {
            return HttpContext.Session.Get<UserEntity>("User") ?? new UserEntity();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            ModelState.Clear();
            if (_repository.FindByLoginUser(model.Login))
            {
                var user = _repository.FindByLoginUser(model.Login, model.Password);
                if (user != null)
                {
                    HttpContext.Session.Set<UserModel>("User", user);
                    return RedirectToRoute("default", new { controller = "", action = "" });
                }
                else
                {
                    ModelState.AddModelError("Password", "Не угадал");
                }
            }
            else
            {
                ModelState.AddModelError("Login", "Таких не знаем");
            }
            return View(model);
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(UserViewModel model)
        {
            ModelState.Clear();
            if (model.Login == null)
            {
                ModelState.AddModelError("Login", "Введите логин");
            }
            if (model.Password == null)
            {
                ModelState.AddModelError("Password", "Введите пароль");
            }
            if (model.FirstName == null)
            {
                ModelState.AddModelError("FirstName", "Кто такой? Чем известен?");
            }
            if (model.LastName == null)
            {
                ModelState.AddModelError("LastName", "Из какой семьи будете? С роду какого?");
            }
            if (model.Phone == null)
            {
                ModelState.AddModelError("Phone", "Введите телефон");
            }
            if (_repository.FindByLoginUser(model.Login))
            {
                ModelState.AddModelError("Login", "Таких уже знаем, другой надо");
            }
            if (ModelState.IsValid)
            {

                _repository.CreateUser(model);
                UserModel user = new UserModel()
                {
                    Id = Guid.NewGuid(),
                    Login = model.Login,
                    Role = "User",
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Phone
                };
                HttpContext.Session.Set<UserModel>("User", user);
                return RedirectToRoute("default", new { controller = "", action = "" });
            }
            return View(model);
        }

        public IActionResult Profile()
        {
            var user = HttpContext.Session.Get<UserModel>("User");
            return View(new UserViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.Phone
            });
        }
        [HttpPost]
        public IActionResult Profile(UserViewModel model)
        {
            var user = HttpContext.Session.Get<UserModel>("User");
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Phone = model.Phone;
            _repository.UpdateUser(user);
            HttpContext.Session.Set<UserModel>("User", user);

            return View(model);
        }

        public IActionResult UserProfile(Guid id)
        {
            var user = HttpContext.Session.Get<UserModel>("User") ?? new UserModel();
            if (user.Role == "Main" || user.Role == "Admin")
            {
                var profile = _repository.FindByIdUser(id);
                return View(new EditUserView()
                {
                    Id = id,
                    Role = profile.Role,
                    Login = profile.Login,
                    FirstName = profile.FirstName,
                    LastName = profile.LastName,
                    Phone = profile.Phone
                });
            }
            else
            {
                return RedirectToRoute("default", new { controller = "", action = "" });
            }
        }

        [HttpPost]
        public IActionResult UserProfile(EditUserView model)
        {
            var user = HttpContext.Session.Get<UserModel>("User") ?? new UserModel();
            if (user.Role == "Main" || user.Role == "Admin")
            {
                if (ModelState.IsValid)
                {
                    var profile = _repository.FindByIdUser(model.Id);
                    profile.Role = model.Role;
                    profile.Login = model.Login;
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;
                    profile.Phone = model.Phone;
                    _repository.UpdateUser(profile);
                    return RedirectToRoute("default", new { controller = "Account", action = "Index" });
                }
                return View(model);
            }
            else
            {
                return RedirectToRoute("default", new { controller = "", action = "" });
            }
        }

        public IActionResult Index()
        {
            var user = HttpContext.Session.Get<UserModel>("User") ?? new UserModel();
            if (user.Role == "Main" || user.Role == "Admin")
            {
                List<UserModel> Users = _repository.GetUser();
                Users.RemoveAll(x => x.Id == user.Id);
                Users.RemoveAll(x => x.Role == "Main");
                return View(Users);
            }
            else
            {
                return RedirectToRoute("default", new { controller = "", action = "" });
            }
        }

        public IActionResult Exit()
        {
            HttpContext.Session.Set<UserModel>("User", new UserModel());
            return RedirectToRoute("default", new { controller = "", action = "" });
        }

        public IActionResult Delete(Guid id)
        {
            var user = HttpContext.Session.Get<UserModel>("User") ?? new UserModel();
            if (user.Role == "Main" || user.Role == "Admin")
            {
                _repository.DeleteUser(id);
                return RedirectToRoute("default", new { controller = "Account", action = "Index" });
            }
            else
            {
                return RedirectToRoute("default", new { controller = "", action = "" });
            }
        }
    }
}
