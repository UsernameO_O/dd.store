﻿using DD.Store.Domain.Repositories;
using DD.Store.Models;
using DD.Store.Models.Account;
using DD.Store.Models.Product;
using DD.Store.Models.Service;
using DD.Store.Models.View;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace DD.Store.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ProductRepository _repository;
        IWebHostEnvironment _appEnvironment;
        public HomeController(ProductRepository repository, ILogger<HomeController> logger, IWebHostEnvironment appEnvironment)
        {
            _logger = logger;
            this._repository = repository;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public JsonResult GetList()
        {
            var obj = _repository.GetProduct();
            var json = JsonConvert.SerializeObject(obj);
            return Json(json);
        }

        [HttpPost]
        public void Delete(Guid id)
        {
            try
            {
                var product = _repository.FindByIdProduct(id);
                var name = (product.Brand + product.Model).Replace(" ", "");
                string path = _appEnvironment.WebRootPath + $"/imageproduct/{name}.png";
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                _repository.DeleteProduct(id);
            }
            catch (Exception ex) { }
        }

        [HttpGet]
        public JsonResult GetProduct(Guid id)
        {
            var json = JsonConvert.SerializeObject(_repository.FindByIdProduct(id));
            return Json(json);
        }

        [HttpGet]
        public List<CategoryModel> GetCategory()
        {
            return _repository.GetCategory();
        }

        public IActionResult Update(Guid id)
        {
            
                ProductModel model = _repository.FindByIdProduct(id);
                List<ProductAttributes> attr = (from i in model.Attributes
                           orderby i.Name
                           select i).ToList();
                CreateViewModel view = new CreateViewModel()
                {
                    Id = id,
                    Brand = model.Brand,
                    Model = model.Model,
                    Category = model.Category,
                    Description = model.Description,
                    ImagePath = model.ImagePath,
                    Price = model.Price,
                    Attributes = attr
                };
                view.CategoryModels = _repository.GetCategory();
            view.Title = "Редактировать";
            TempData["ProductCrUp"] = JsonConvert.SerializeObject(view);
            return View(view);
            
        }
        public IActionResult Create()
        {
            CreateViewModel view = new CreateViewModel();
            view.CategoryModels = _repository.GetCategory();
            view.Id = Guid.NewGuid();
            view.Title = "Создать";
            TempData["ProductCrUp"] = JsonConvert.SerializeObject(view);
            return View(view);
        }
        [HttpPost]
        public IActionResult Create(CreateViewModel view)
        {
            try
            {
                var viewProduct = JsonConvert.DeserializeObject<CreateViewModel>(TempData["ProductCrUp"].ToString());
                view.Title = viewProduct.Title;
                #region Валидация
                ModelState.Clear();
                if (view.Brand == null)
                {
                    ModelState.AddModelError("Brand", "Введите бренд");
                }
                if (view.Model == null)
                {
                    ModelState.AddModelError("Model", "Введите модель");
                }
                if (view.Description == null || view.Description.Length < 4)
                {
                    ModelState.AddModelError("Description", "Опишите продукт подробнее");
                }
                if (view.Price == null || view.Price == 0)
                {
                    ModelState.AddModelError("Price", "Введите цену");
                }
                if (view.FileImage == null && viewProduct.ImagePath == null)
                {
                    ModelState.AddModelError("FileImage", "Выберите файл");
                }
                if (view.Category == null)
                {
                    ModelState.AddModelError("Category", "Выберите категорию");
                }
                else
                {
                    var attributes = _repository.GetCategory().FirstOrDefault(c => c.Name == view.Category).Attributes;
                    if (view.Attributes != null)
                    {
                        for (int i = 0; i < attributes.Count; i++)
                        {
                            view.Attributes[i].Description = attributes[i].Description;
                            view.Attributes[i].Name = attributes[i].Name;
                            if (view.Attributes[i].Value == null)
                            {
                                ModelState.AddModelError($"Attributes[{i}].Value", "Задайте характеристику");
                            }
                        }
                    }
                }
                #endregion
                if (ModelState.IsValid)
                {
                    var name = (view.Brand + view.Model).Replace(" ", "");
                    string path = "";
                    if (view.FileImage != null)
                    {
                        foreach (var file in view.FileImage)
                        {
                            path = $"/imageproduct/{name}.png";
                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                            }
                            using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                            {
                                file.CopyToAsync(fileStream);
                            }
                        }
                    }
                    else
                    {
                        path = viewProduct.ImagePath;
                    }
                    var attr = (from i in view.Attributes orderby i.Name select i).ToList();
                    ProductModel product = new ProductModel()
                    {
                        Id = viewProduct.Id,
                        Category = view.Category,
                        ImagePath = path,
                        Brand = view.Brand,
                        Model = view.Model,
                        Price = (int)view.Price,
                        Description = view.Description,
                        Attributes = attr
                    };
                    _repository.SaveProduct(product);
                    return RedirectToRoute("default", new { controller = "", action = "" });
                }
                else
                {
                    view.CategoryModels = _repository.GetCategory();
                    return View(view);
                }
            }
            catch (Exception ex)
            {
                return RedirectToRoute("default", new { controller = "", action = "" });
            }
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Show(Guid id)
        {
            ProductModel? porduct = _repository.FindByIdProduct(id);
            return View(porduct);
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}