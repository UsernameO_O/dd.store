﻿using DD.Store.Domain.Repositories;
using DD.Store.Models.Product;
using DD.Store.Models.View;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;

namespace DD.Store.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ProductRepository _repository;
        public CategoryController(ProductRepository repository, ILogger<HomeController> logger)
        {
            _logger = logger;
            this._repository = repository;
        }

        public IActionResult Index()
        {
            var categories = _repository.GetCategory();
            return View(categories);
        }

        public IActionResult Delete(Guid id)
        {
            _repository.DeleteCategory(id);
            return RedirectToRoute("default", new { controller = "Category", action = "Index" });
        }

        public IActionResult Update(Guid id)
        {
            var category = _repository.GetCategory().FirstOrDefault(x => x.Id == id);
            TempData["CategoryCrUp"] = JsonConvert.SerializeObject(category);
            return View(category);
        }

        public IActionResult Create()
        {
            CategoryModel category = new CategoryModel();
            category.Id = Guid.NewGuid();
            TempData["CategoryCrUp"] = JsonConvert.SerializeObject(category);
            return View();
        }

        [HttpPost]
        public IActionResult Create(CategoryModel model)
        {
            var modelCategory = JsonConvert.DeserializeObject<CreateViewModel>(TempData["CategoryCrUp"].ToString());
            ModelState.Clear();
            if (model.Description == null)
            {
                ModelState.AddModelError("Description", "Введите название");
            }
            else if (isName(model.Description))
            {
                ModelState.AddModelError("Description", "Используйте только русские буквы");
            }
            if (model.Name == null)
            {
                ModelState.AddModelError("Name", "Введите ключевое слово");
            }
            else if (isKeyword(model.Name))
            {
                ModelState.AddModelError("Name", "Используйте только латинские буквы");
            }
            model.Id = modelCategory.Id;
            if (model.Attributes != null)
            {
                for (int i = 0; i < model.Attributes.Count; i++)
                {
                    if (model.Attributes[i].Description == null)
                    {
                        ModelState.AddModelError($"Attributes[{i}].Description", "Введите название");
                    }
                    else if (isName(model.Attributes[i].Description))
                    {
                        ModelState.AddModelError($"Attributes[{i}].Description", "Используйте только русские буквы");
                    }
                    if (model.Attributes[i].Name == null)
                    {
                        ModelState.AddModelError($"Attributes[{i}].Name", "Введите ключевое слово");
                    }
                    else if (isKeyword(model.Attributes[i].Name))
                    {
                        ModelState.AddModelError($"Attributes[{i}].Name", "Используйте только латинские буквы");
                    }
                    model.Attributes[i].Id = Guid.NewGuid();
                }
            }
            if(ModelState.IsValid)
            {
                _repository.SaveCategory(model);
            }
            return View(model);
        }

        private bool isKeyword(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (((str[i] >= 'a') && (str[i] <= 'z')) ||
                    ((str[i] >= 'A') && (str[i] <= 'Z'))) return false;
            }
            return true;
        }
        private bool isName(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (((str[i] >= 'а') && (str[i] <= 'я')) ||
                    ((str[i] >= 'А') && (str[i] <= 'Я'))) return false;
            }
            return true;
        }

    }
}
