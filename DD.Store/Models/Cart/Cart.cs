﻿namespace DD.Store.Models.Cart
{
    public class Cart
    {
        public List<CartItemModel> CartItems { get; set; } = new List<CartItemModel>();

        public CartItemModel? FindByIdCarts(Guid id)
        {
            return CartItems?.FirstOrDefault(x => x.Id == id);
        }
        public CartItemModel? FindByProductIdCarts(Guid id)
        {
            return CartItems?.FirstOrDefault(x => x.Product.Id == id);
        }

        public void Add(CartItemModel entity)
        {
            if (FindByIdCarts(entity.Id) == null)
            {
                CartItems.Add(entity);
            }
            else
            {
                var product = FindByIdCarts(entity.Id);
                product = entity;
            }
        }

        public void Delete(Guid id)
        {
            var product = FindByProductIdCarts(id);
            CartItems.Remove(product);
        }

        public int GetTotalNumber()
        {
            int toReturn = 0;
            foreach (var item in CartItems)
                toReturn += item.Number;
            return toReturn;
        }

        public int GetTotalPrice()
        {
            int toReturn = 0;
            foreach (var item in CartItems)
                toReturn += item.Product.Price * item.Number;
            return toReturn;
        }
    }
}
