﻿using DD.Store.Models.Product;

namespace DD.Store.Models.Cart
{
    public class CartItemModel
    {
        public Guid Id { get; set; }
        public ProductModel Product { get; set; }
        public int Number { get; set; } = 1;
        public void NumberUp() => Number++;
        public void NumberDown() => Number--;
    }
}
