﻿using DD.Store.Models.View;

namespace DD.Store.Models.Cart
{
    public class OrderModel
    {
        public ClientViewModel Client { get; set; }
        public Cart Cart { get; set; }

        // для того чтобы по возвращения после неудачной валидации открыть модальное окно
        public bool isValid { get; set; }
    }
}
