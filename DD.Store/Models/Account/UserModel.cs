﻿namespace DD.Store.Models.Account
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
