﻿namespace DD.Store.Models.Product
{
    public class ProductAttributes
    {
        public string? Name { get; set; }
        public string? Value { get; set; }
        public string? Description { get; set; }
        public string? Type { get; set; } // единицы измерений
        public string? DataType { get; set; }
    }
}
