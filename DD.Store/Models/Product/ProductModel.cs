﻿namespace DD.Store.Models.Product
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        public string? Category { get; set; }
        public string? ImagePath { get; set; }
        public string? Brand { get; set; }
        public string? Model { get; set; }
        public int Price { get; set; }
        public string? Description { get; set; }
        public List<ProductAttributes>? Attributes { get; set; }
    }
}
