﻿using DD.Store.Domain.Entities;

namespace DD.Store.Models.Product
{
    public class CategoryModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<AttributesEntity> Attributes { get; set; }
    }
}
