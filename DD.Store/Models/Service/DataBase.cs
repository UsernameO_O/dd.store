﻿namespace DD.Store.Models.Service
{
    public class DataBase
    {
        public static string ConnectionStringProducts { get; set; }
        public static string ConnectionStringUsers { get; set; }
    }
}
