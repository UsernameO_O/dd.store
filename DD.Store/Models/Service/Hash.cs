﻿using System.Security.Cryptography;
using System.Text;

namespace DD.Store.Models.Service
{
    public static class Hash
    {
        public static string Encryption(string passwordString)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(passwordString);
            byte[] passwordHash = SHA256.HashData(passwordBytes);
            string password = Convert.ToHexString(passwordHash);
            return password;
        }
    }
}
