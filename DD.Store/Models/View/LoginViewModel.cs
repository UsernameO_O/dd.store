﻿using System.ComponentModel.DataAnnotations;

namespace DD.Store.Models.View
{
    public class LoginViewModel
    {
        [Display(Name = "Логин")]
        public string Login { get; set; }


        [Display(Name = "Пароль")]
        [UIHint("password")]
        public string Password { get; set; }
    }
}
