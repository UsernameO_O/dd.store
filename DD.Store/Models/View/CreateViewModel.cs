﻿using DD.Store.Models.Product;
using System.ComponentModel.DataAnnotations;

namespace DD.Store.Models.View
{
    public class CreateViewModel
    {
        [Display(Name = "Бренд")]
        public string? Brand { get; set; }
        [Display(Name = "Модель")]
        public string? Model { get; set; }
        [Display(Name = "Категория")]
        public string? Category { get; set; }
        [Display(Name = "Описание")]
        public string? Description { get; set; }
        [Display(Name = "Цена")]
        public int? Price { get; set; }
        public string? ImagePath { get; set; }
        public Guid Id { get; set; }
        public List<CategoryModel> CategoryModels { get; set; }
        public IFormFileCollection FileImage { get; set; }
        public List<ProductAttributes> Attributes { get; set; }

        public string Title { get; set; }
    }
}
