﻿using System.ComponentModel.DataAnnotations;

namespace DD.Store.Models.View
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        [UIHint("password")]
        public string Password { get; set; }
        public string Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
