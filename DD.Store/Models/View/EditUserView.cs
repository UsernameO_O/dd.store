﻿using System.ComponentModel.DataAnnotations;

namespace DD.Store.Models.View
{
    public class EditUserView
    {
        public Guid Id { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Укажите имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Укажите фамилию")]
        public string LastName { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Укажите телефон")]
        public string Phone { get; set; }

        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Укажите логин")]
        public string Login { get; set; }

        [Display(Name = "Роль")]
        [Required(ErrorMessage = "Укажите роль")]
        public string Role { get; set; }
    }
}
