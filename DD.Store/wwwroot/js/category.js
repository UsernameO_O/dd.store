function CreateAttributes() {

    listElement = document.querySelectorAll(`.category-line`)
    i = listElement.length-2
    let field = `
    <div class="category-line">
        <div class="register-field">
            <input type="text" name="Attributes[${i}].Description" placeholder="Name" />
        </div>
        <span class="validation" asp-validation-for="@Model.Attributes[${i}].Description"></span>

        <div class="register-field">
        <input type="text" name="Attributes[${i}].Name" placeholder="Key Name" />
        </div>
        <span class="validation" asp-validation-for="@Model.Attributes[${i}].Name"></span>

        <select class="register-field register-select" name="Attributes[${i}].DataType">
            <option class="option-init" value="string">string</option>
            <option class="option-init" value="int">int</option>
        </select>
        <span class="validation" asp-validation-for="@Model.Attributes[${i}].DataType"></span>

        <div class="register-field">
        <input type="text" name="Attributes[${i}].Type" placeholder="inType" />
        </div>
        <span class="validation" asp-validation-for="@Model.Attributes[${i}].Type"></span>
    </div>`

    document.querySelector('.category-attribute-container').innerHTML += field
}
``