﻿var user;
function InitUser() {
    const request = new XMLHttpRequest();
    request.open("GET", "/account/getuser");
    request.setRequestHeader("Content-type", "application/json; charset=utf-8");
    request.onload = function () {
        user = JSON.parse(request.response);
        if (user.id == "00000000-0000-0000-0000-000000000000") {
            document.querySelector(`.header-user`).innerHTML = `<div id="button-user" onclick="Login()">Вход</div>`;
        }
        else {
            document.querySelector(`.header-user`).innerHTML = `<div id="button-user" onclick="AccountMenu()">${user.firstName} ${user.lastName}</div>`;
        }

        if (user.role == 'Main' || user.role == 'Admin') {
            document.querySelector('.account-menu').innerHTML = `<a id="button-account" class="button-action-account" href="/account/profile">
                <img class="icon-menu" loading="lazy" fetchpriority="low"
                     src="/image/icon-account.png">
                <span>Профиль</span>
            </a>
            <a id="button-root" class="button-action-account" href="/account/index">
                <img class="icon-menu" loading="lazy" fetchpriority="low"
                     src="/image/icon-root.png">
                <span>Администрирование</span>
            </a>
            <a id="button-exit" class="button-action-account" href="/account/exit">
                <img class="icon-menu" loading="lazy" fetchpriority="low"
                     src="/image/icon-exit.png">
                <span>Выход</span>
            </a>`;
        }
    };
    request.send();
}
InitUser();


headHeight = document.querySelector('.header').clientHeight;
document.querySelector('body').style = `margin-top: ${headHeight}px`;

function Home() {
    window.location.href = `/`
}

function AccountMenu() {
    headHeight = document.querySelector('#button-user').clientHeight;
    document.querySelector('.modal-account').classList.toggle('open');
}

function Login() {
    window.location.href = "/Account/Login"
}