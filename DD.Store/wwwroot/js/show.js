﻿const request = new XMLHttpRequest();
request.open("GET", "/home/GetProduct");
request.setRequestHeader("Content-type", "application/json; charset=utf-8");
request.onload = function () {
    element = JSON.parse(JSON.parse(request.response));
    InitProduct();
};
request.send();

function InitProduct() {
    product = `
    <div class="element product">
        <div class="product-image-detail">
            <div class="flex-image-container">
                <img class="image-flex-item" loading="lazy" fetchpriority="low"
                     src="https://ir.ozone.ru/s3/multimedia-r/wc1000/6616750851.jpg">
                <img class="image-flex-item" loading="lazy" fetchpriority="low"
                     src="https://ir.ozone.ru/s3/multimedia-4/wc1000/6616750864.jpg">
                <img class="image-flex-item" loading="lazy" fetchpriority="low"
                     src="https://ir.ozone.ru/s3/multimedia-v/wc1000/6616750855.jpg">
                <img class="image-flex-item" loading="lazy" fetchpriority="low"
                     src="https://ir.ozone.ru/s3/multimedia-s/wc1000/6616750852.jpg">
            </div>
            <div class="product-image-avatar">
                <img loading="lazy" fetchpriority="low"
                     src="">
            </div>
        </div>
        <div class="product-info">
            <div class="product-name">
                ${element.Brand}
            </div>
            <div class="product-description">
                <font color="#808080">Диагональ:</font> 7<br>
                <font color="#808080">Емкость аккумулятора:</font> 5000<br>
                <font color="#808080">Стандарты связи:</font> 4G<br>
                <font color="#808080">Материал корпуса:</font> Алюминий<br>
            </div>
        </div>
        <div class="product-sidebar">
            <div class="product-price">
                21000₽
            </div>
            <batton type="button" class="button-card"><p>Купить</p></batton>
        </div>
    </div>
    `
    document.querySelector('.container-product').innerHTML = product;
}