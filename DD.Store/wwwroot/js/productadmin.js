﻿const request = new XMLHttpRequest();
request.open("GET", "/home/GetCategory");
request.setRequestHeader("Content-type", "application/json; charset=utf-8");
request.onload = function () {
    entity = JSON.parse(request.response);
};
request.send();





function CreateAttributes() {
    var select = document.querySelector('.register-select').value
    var container = ''
    if (select != '') {
        document.querySelector('.register-select').classList.remove('option-init')
        category = entity.find(x => x.name == select)
        i = 0
        
        category.attributes.forEach((att) => {
            dataType = att.dataType == 'int' ? 'type = "number"' : ''
            typeValue = att.type != '' ? `, ${att.type}` : ''
            container += `
            <div class="register-field">
                    <input ${dataType} name="Attributes[${i}].Value" placeholder="${att.description}${typeValue}" />
            </div>
            <span class="validation" asp-validation-for="Attributes[${i}].Value"></span>
                `
                i++
        });
    }
    else {
        document.querySelector('.register-select').classList.add('option-init');
    }
    document.querySelector('#container-attributes').innerHTML = container
}
