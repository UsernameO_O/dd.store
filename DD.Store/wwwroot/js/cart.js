﻿function Delete(id) {

    var xhr = new XMLHttpRequest();
    xhr.open('POST', `/cart/delete/${id}`);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
    var elem = document.querySelector(`#id-${id}`)
    elem.parentNode.removeChild(elem);
    if (document.querySelector('.container-product div') == null) window.location.href = '/';
    GlobalPrice()
    GlobalNumber()
}

function NumberUp(id) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', `/cart/NumberUp/${id}`);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();

    number = Number(document.querySelector(`#id-${id} .count-value`).value) + 1;
    document.querySelector(`#id-${id} .count-value`).value = number;
    price = document.querySelector(`#id-${id} .product-price`).dataset.price;
    document.querySelector(`#id-${id} .product-price`).innerHTML = number * price + '₽';
    GlobalPrice()
    GlobalNumber()
}

function NumberDown(id) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', `/cart/NumberDown/${id}`);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();

    number = Number(document.querySelector(`#id-${id} .count-value`).value) - 1;
    document.querySelector(`#id-${id} .count-value`).value = number;
    if (document.querySelector(`#id-${id} .count-value`).value == 0) Delete(id);
    price = document.querySelector(`#id-${id} .product-price`).dataset.price;
    document.querySelector(`#id-${id} .product-price`).innerHTML = number * price + '₽';
    GlobalPrice()
    GlobalNumber()
}



function OpenModal() {
    document.querySelector('#order-modal').classList.add("open");
}

function CloseModal() {
    document.querySelector('#order-modal').classList.remove("open");
}

function Input(id) {
    number = Number(document.querySelector(`#id-${id} .count-value`).value);
    document.querySelector(`#id-${id} .count-value`).value = number;
    price = document.querySelector(`#id-${id} .product-price`).dataset.price;
    document.querySelector(`#id-${id} .product-price`).innerHTML = number * price + '₽';
    GlobalPrice()
    GlobalNumber()
}

function GlobalPrice() {
    listElement = document.querySelectorAll(`.product-price`)
    globalPrice = 0;
    for (let i in listElement) {
        price = parseInt(listElement[i].innerHTML)
        if (/^\d+$/.test(price)) {
            globalPrice += price
        }
    }
    document.querySelector(`.global-price`).innerHTML = globalPrice;
}

function GlobalNumber() {
    listElement = document.querySelectorAll(`.count-value`)
    globalNumber = 0;
    for (let i in listElement) {
        number = parseInt(listElement[i].value)
        if (/^\d+$/.test(number)) {
            globalNumber += number
        }
    }
    document.querySelector(`.global-number`).innerHTML = globalNumber;
}

function Change(id) {
    number = Number(document.querySelector(`#id-${id} .count-value`).value);
    if (number == 0) {
        Delete(id)
    }
    else {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', `/cart/NumberSet?id=${id}&value=${number}`);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send();
    }

}