﻿(function InitCategory() {
    const request = new XMLHttpRequest();
    request.open("GET", "home/GetCategory");
    request.setRequestHeader("Content-type", "application/json; charset=utf-8");
    request.onload = function () {
        categories = JSON.parse(request.response);
        InitFilter()
    };
    request.send();
})()

function InitAdmin() {
    if (user.role == 'Main' || user.role == 'Admin') {
        document.querySelector('.button-create-product').classList.add('open');
    }

}

(function InitUser() {
    const request = new XMLHttpRequest();
    request.open("GET", "account/getuser");
    request.setRequestHeader("Content-type", "application/json; charset=utf-8");
    request.onload = function () {
        user = JSON.parse(request.response);
        InitAdmin();
    };
    request.send();
})()

enumerable = []
function InitSite() {

    const request = new XMLHttpRequest();
    request.open("GET", "home/getlist");
    request.setRequestHeader("Content-type", "application/json; charset=utf-8");
    request.onload = function () {
        enumerable = JSON.parse(JSON.parse(request.response));
        InitValueAttributes()
        InitPrice();
        InitSort();
    };
    request.send();
}

(function InitCart() {
    inCart = []
    const request = new XMLHttpRequest();

    request.open("GET", "/cart/getcart");
    request.setRequestHeader("Content-type", "application/json; charset=utf-8");
    request.onload = function () {
        cart = JSON.parse(request.response);
        document.querySelector(`.button-cart-shop span`).innerHTML = cart.length;
        for (i = 0; i < cart.length; i++) inCart.push(cart[i].product.id);
        InitSite();
    };
    request.send();
})()

function InitPrice() {
    enumerable.sort((a, b) => a.Price > b.Price ? 1 : -1);
    priceMin = enumerable[0].Price;
    document.querySelector('#price-min').placeholder = `от ${priceMin}`;

    enumerable.sort((a, b) => a.Price < b.Price ? 1 : -1);
    priceMax = enumerable[0].Price;
    document.querySelector('#price-max').placeholder = `до ${priceMax}`;
}

function InitSort(act = 'price-down') {
    optionSorting = document.querySelectorAll('.option-sorting')
    for (i = 0; i < optionSorting.length; i++) {
        optionSorting[i].style = "color: rgb(117,117,117); font-weight: 400;";
    }
    switch (act) {
        case 'price-down':
            enumerable.sort((a, b) => a.Price > b.Price ? 1 : -1);
            document.querySelector('#price-down').style = "color: black; font-weight: 500;";
            InitProduct();
            break;
        case 'price-up':
            enumerable.sort((a, b) => a.Price < b.Price ? 1 : -1);
            document.querySelector('#price-up').style = "color: black; font-weight: 500;";
            InitProduct();
            break;
        case 'name-down':
            enumerable.sort((a, b) => a.Brand > b.Brand ? 1 : -1);
            document.querySelector('#name-down').style = "color: black; font-weight: 500;";
            InitProduct();
            break;
        case 'name-up':
            enumerable.sort((a, b) => a.Brand < b.Brand ? 1 : -1);
            document.querySelector('#name-up').style = "color: black; font-weight: 500;";
            InitProduct();
            break;
    }
}

attributes = []
function InitValueAttributes() {
    for (i in enumerable) {
        for (j in enumerable[i].Attributes) {
            attributes.push({
                name: enumerable[i].Attributes[j].Name,
                value: enumerable[i].Attributes[j].Value,
            })
        }
    }
}
categoryFilter = []
function InitFilter() {
    
    ctgContainer = ''
    if (_category == 'missing') {
        
        categories.forEach((ctg) => {
            ctgContainer += `<div onclick="InitCategory('${ctg.name}')" id="${ctg.name}" class="option-category-filters"><p>${ctg.description}</p></div>`
        });
        if (user.role == 'Main' || user.role == 'Admin') {
            ctgContainer += `<div onclick="CreateCategory()" class="option-category-filters"><p>Редактировать</p></div>`
        }
    }
    else {
        categoryFilter = []
        ctgFocus = categories.find(x => x.name == _category)
        ctgContainer += `<div onclick="InitCategory('${ctgFocus.name}')" id="${ctgFocus.name}" class="option-category-filters"><p>${ctgFocus.description}</p></div>`

        
        for (i = 0; i < ctgFocus.attributes.length; i++){
            atValues = []
            attributes.forEach(attr => {
                if (attr.name == ctgFocus.attributes[i].name) {
                    atValues.push(attr.value)
                }
            })
            var stack = []
            var type = ''
            if (ctgFocus.attributes[i].dataType == 'int') {
                atValues.sort((a, b) => a > b ? 1 : -1);
                attrMin = atValues[0];
                
                atValues.sort((a, b) => a < b ? 1 : -1);
                attrMax = atValues[0];
                type += `
                <div class="container-price">
                    <input id="min" data-nvalue="${attrMin}" name="${ctgFocus.attributes[i].name}" oninput="InitInt(this)" placeholder="от ${attrMin}">
                    <input id="max" data-nvalue="${attrMax}" name="${ctgFocus.attributes[i].name}" oninput="InitInt(this)" placeholder="до ${attrMax}">
                </div>`
                categoryFilter.push({ name: ctgFocus.attributes[i].name, type: 'int', min: attrMin, max: attrMax})
            }
            else if (ctgFocus.attributes[i].dataType == 'string') {
                atValues.forEach((values) => {
                    
                    if (!stack.includes(values.toUpperCase())) {
                        stack.push(values.toUpperCase())
                    type += `
                    <div data-name="${ctgFocus.attributes[i].name}" data-value="${values}" id="Value-${values}" onclick="InitString(this)" class="option-filters">
                        <input type="checkbox" class="input-filter" checked><span class="text-filters" >${values}</span>
                    </div>`
                        categoryFilter.push({ name: ctgFocus.attributes[i].name, type: 'string', value: values })
                    }
                    
                })
            }
                ctgContainer += `<div class="container-product-filters">
                <div class="option-attribute-filters"><p>${ctgFocus.attributes[i].description}</p></div>
                ${type}
                 </div>`
        }
    }
    document.querySelector('.container-filters').innerHTML = ctgContainer
}

function PriceRange(price) {
    min = document.querySelector('#price-min').value;
    if (min == '') min = priceMin;
    max = document.querySelector('#price-max').value;
    if (max == '') max = priceMax;
    if (min <= price && max >= price) return true;
    else return false;
}

function InitString(native) {
    if (document.querySelector(`#${native.id} input`).checked == true) {
        let attributeName = categoryFilter.filter(x => x.name == native.dataset.name)
        let attributeValue = attributeName.find(x => x.value == native.dataset.value)
        let index = categoryFilter.findIndex(x => x == attributeValue)
        categoryFilter.splice(index, 1);
    }
    else {
        categoryFilter.push({ name: native.dataset.name, type: 'string', value: native.dataset.value })
    }
    document.querySelector(`#${native.id} input`).checked = !document.querySelector(`#${native.id} input`).checked;
    InitProduct()
};
function InitInt(native) {
    let attribute = categoryFilter.find(x => x.name == native.name)
    if (native.id == 'min') {
        attribute.min = native.value == '' ? native.dataset.nvalue : native.value
    }
    else if (native.id == 'max') {
        attribute.max = native.value == '' ? native.dataset.nvalue : native.value
    }
    InitProduct();
}



function InitFilterAttributes(product) {
    if (_category == 'missing') return true;
    if (_category == product.Category) {
        for (let i in product.Attributes) {
            let filterAttr = categoryFilter.find(x => x.name == product.Attributes[i].Name)
            if (filterAttr == undefined) return false;
            if (filterAttr.type == 'string') {
                stringFilter = categoryFilter.filter(x => x.name == product.Attributes[i].Name)
                let value = stringFilter.find(x => x.value == product.Attributes[i].Value)
                if (value == undefined)
                    return false;
            }
            else if (filterAttr.type == 'int') {
                if (Number(filterAttr.min) > Number(product.Attributes[i].Value))
                    return false;
                if (Number(filterAttr.max) < Number(product.Attributes[i].Value))
                    return false;
            }
        }
        return true;
    }
    return false;
}


function InitCategory(category = null) {
    if (_category == category) _category = 'missing';
    else if (category != null) {
        _category = category;
    }
    InitFilter();
    InitProduct();
}
_category = 'missing'
function InitProduct() {
    product = ''
    for (i = 0; i < enumerable.length; i++) {

        valueCart = inCart.find((a) => a == enumerable[i].Id) != undefined ? "В корзине" : "Купить"
        productName = (enumerable[i].Brand + enumerable[i].Model).toUpperCase();
        inputString = document.querySelector('#input-search').value.toUpperCase();
        isInclude = productName.includes(inputString);

        

        
        isFilter = InitFilterAttributes(enumerable[i]);
        if (PriceRange(enumerable[i].Price) && isInclude && isFilter) {
            description = ''
            enumerable[i].Attributes.forEach((att) => {
                description += `<font color="#808080">${att.Description}${att.Type != '' ? `, ${att.Type}` : '' }:</font>  ${att.Value}<br>`
            });
            var admin = '';
            if (user.role == 'Main' || user.role == 'Admin') admin = `
            <batton type="button" onclick="Delete('${enumerable[i].Id}')" id="del${enumerable[i].Id}" class="button-cart"><p>Удалить</p></batton>
            <batton type="button" onclick="Update('${enumerable[i].Id}')" id="edit${enumerable[i].Id}" class="button-cart"><p>Изменить</p></batton>
            `
            product += `
                <div id="product-${enumerable[i].Id}" class="element product">
                    <div class="product-image">
                        <img loading="lazy" fetchpriority="low"
                             src="${enumerable[i].ImagePath}">
                    </div>
                    <div class="product-info">
                        <div onclick="OpenProduct('${enumerable[i].Id}')" class="product-name">
                            ${enumerable[i].Brand} ${enumerable[i].Model}
                        </div>
                        <div class="product-description">
                            ${description}
                        </div>
                    </div>
                    <div class="product-sidebar">
                        <div class="product-price">
                            ${enumerable[i].Price} ₽
                        </div>
                        <div class="product-button">
                            ${admin}
                            <batton type="button" onclick="InCart('${enumerable[i].Id}')" id="id${enumerable[i].Id}" class="button-cart"><p>${valueCart}</p></batton>
                        </div>
                    </div>
                </div>
            `
        }
    }
    document.querySelector('.container-products').innerHTML = product;
}




function OpenProduct(id) {
    window.location.href = `/Home/Show/${id}`
}

function OpenCart() {
    if (document.querySelector(`.button-cart-shop span`).innerHTML != 0) window.location.href = `/Cart/Index`
}


function InCart(id) {
    switch (document.querySelector(`#id${id} p`).innerHTML) {
        case 'Купить':
            document.querySelector(`#id${id} p`).innerHTML = 'В корзине'
            document.querySelector(`.button-cart-shop span`).innerHTML = Number(document.querySelector(`.button-cart-shop span`).innerHTML) + 1
            var xhr = new XMLHttpRequest();
            xhr.open('POST', `cart/Create/${id}`);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send();
            inCart.push(id)
            break;
        case 'В корзине':
            document.querySelector(`#id${id} p`).innerHTML = 'Купить'
            document.querySelector(`.button-cart-shop span`).innerHTML = Number(document.querySelector(`.button-cart-shop span`).innerHTML) - 1
            var xhr = new XMLHttpRequest();
            xhr.open('POST', `cart/Delete/${id}`);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send();
            index = inCart.indexOf(id);
            if (index !== -1) {
                inCart.splice(index, 1);
            }
            break;
    }
}

function Delete(id) {
    document.querySelector(`#product-${id}`).remove();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', `Home/Delete/${id}`);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
}

function Update(id) {
    window.location.href = `/Home/Update/${id}`
}

function CreateProduct(id) {
    window.location.href = `/Home/Create/${id}`
}

function CreateCategory(id) {
    window.location.href = `Category/Index`
}