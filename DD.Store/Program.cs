using DD.Store.Domain;
using DD.Store.Domain.Repositories;
using DD.Store.Models;
using DD.Store.Models.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.Bind("DataBase", new DataBase());
// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddTransient<ProductRepository>();
builder.Services.AddTransient<UserRepository>();
builder.Services.AddTransient<ProductContext>();
builder.Services.AddTransient<UserContext>();

builder.Services.AddDistributedMemoryCache();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSession(options =>
    options.IdleTimeout = TimeSpan.FromMinutes(120)
);
builder.Services.AddDbContext<ProductContext>(options =>
{
    options.UseSqlServer(DataBase.ConnectionStringProducts);
    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});
builder.Services.AddDbContext<UserContext>(options =>
{
    options.UseSqlServer(DataBase.ConnectionStringUsers);
    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.UseSession();
app.Run();
